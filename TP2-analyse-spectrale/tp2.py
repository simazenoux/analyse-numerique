import numpy as np
import matplotlib.pyplot as plt
import math

def q1(A, vect, eps):
    y = vect
    lambAnc = 1
    lamb = 0
    while abs(lamb - lambAnc) > eps :
        lambAnc = lamb
        x = y/np.linalg.norm(y)
        y = A*x
        lamb = (np.matrix(x).T*y)/(np.matrix(x).T*x)
    print(lamb)
    print(x)




def q2(A, vect, eps):
    y = vect
    lambAnc = 1
    lamb = 0
    trace = np.array([])
    i = 0
    it = np.array([])
    while abs(lamb - lambAnc) > eps :
        i = i + 1
        it = np.append(it,i)
        val = np.log10(abs(lamb - lambAnc))
        trace = np.append(trace,val)
        lambAnc = lamb
        x = y/np.linalg.norm(y)
        y = A*x
        lamb = (np.matrix(x).T*y)/(np.matrix(x).T*x)
    
    fig, ax = plt.subplots()
    ax.plot(it, trace)
    plt.show()

A=np.matrix([[3,4,0],[1,3,0],[0,0,2]])
eps=1E-15 
x0= np.matrix([2,1,1]).T


#q1(A,A*x0,eps)
q2(A,A*x0,eps)