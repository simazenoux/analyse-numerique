import math
import numpy as np
import numpy.linalg as lg



def puissanceIteree(A, x0, eps):
    x = x0
    lanc = 1
    l = 0
    while abs(l-lanc) > eps:
        lanc = l
        Ax = A*x
        x = Ax / lg.norm(Ax)
        l = (np.matrix(x).T * A*x) / (np.matrix(x).T * x)
    return (x,l)

def puissanceIteree2(A, y0, eps):
    y = y0
    lanc = 1
    l = 0
    while (abs(l-lanc) > eps):
        lanc = l
        x = y/lg.norm(y)
        y = A*x
        l = np.matrix(x).T * y / (np.matrix(x).T * x)
    return (x, l)


A=np.matrix([[3,4,0],[1,3,0],[0,0,2]])
x0= np.matrix([2,1,1]).T
eps=1E-15

print(puissanceIteree(A, x0, eps))
print(puissanceIteree2(A, x0, eps))
print(puissanceIteree2(A, A*x0, eps))
print(A.shape[0])


#Q3
def deflation(A, x0, eps):
    res = []
    v, l = puissanceIteree2(A, x0, eps)
    B = A - l*v*np.matrix(v).T / (np.matrix(v).T * v)
    return (res)

print(deflation(A, x0, eps))
