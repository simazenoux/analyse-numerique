import numpy as np

#Q1

def evalQuadratic(A,b,x):
    return (0.5*np.dot(np.dot(np.transpose(x),A),x) - np.dot(np.transpose(b),x))[0][0]

def gradiantQuadratic(A,b,x):
    return np.dot(A,x) - b

def hessienneQuadratic(A):
    return A

def quadratic(A,b,x,order=0):
    res = []
    res.append(evalQuadratic(A,b,x))
    if order>=1:
        res.append(gradiantQuadratic(A,b,x))
    if order == 2:
        res.append(hessienneQuadratic(A))
    return res

A=[[1,2],[3,4]]
b=[[-4],[-4]]
x=[[3],[3]]

print(quadratic(A,b,x,2))

print(evalQuadratic(A,b,x))
print(gradiantQuadratic(A,b,x))
print(hessienneQuadratic(A))

#Q4
def descente_gradiant_pas_optimal_quadratique(A,b,initial_x,eps=1e-15, maximum_iterations=100):
    x = initial_x
    g = np.dot(A,x) - b
    while  np.dot(np.transpose(g),g) > eps and maximum_iterations >=0:
        g = np.dot(A,x) - b
        t = np.dot(np.transpose(g),g) / np.dot(np.dot(np.transpose(g),A),g)
        x = x- t*g
        maximum_iterations -= 1
    return x

A=[[1,2],[3,4]]
b=[[-4],[-4]]
x=[[2],[3]]
print("Descente gradiant pas optimal quadratique :")
print(descente_gradiant_pas_optimal_quadratique(A,b,x))

#Q5
def gradient_conjugue_quadratique(A,b,initial_x,eps=1e-15):
    x = initial_x
    g = np.dot(A,x) - b
    d = -g
    while np.linalg.norm(g) >= eps:
        t = np.dot(g.T,d) / np.dot(np.dot(d.T,A),d)
        x += t*d
        g = np.dot(A,x) - b
        b = np.dot(np.dot(g.T,A),d) / np.dot(np.dot(d.T,A),d)
        d = -g + b*d
    return x
    
A=[[1,2],[3,4]]
b=[[-4],[-4]]
x=[[2],[3]]
gradient_conjugue_quadratique(A,b,x)