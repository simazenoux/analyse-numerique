import numpy as np
import matplotlib.pyplot as plt
import time
import random as rd

def dichotomie (f,a,b,epsilon): #Q2
    xc=a+(2/4)*(b-a)
    fxc=f(xc)
    xg=a+(1/4)*(b-a)
    xd=a+(3/4)*(b-a)
    fxg=f(xg)
    fxd=f(xd)
    while xd-xg > epsilon:
        if fxc>fxd:
            a=xc
            xc=xd
            fxc=fxd
        elif fxc>fxg:
            b=xc
            xc=xg
            fxc=fxg
        else:
            a=xg
            b=xd
        xg=a+(1/4)*(b-a)
        xd=a+(3/4)*(b-a)
        fxg=f(xg)
        fxd=f(xd)
    return (xc)

def f1(x): return abs(x-100)
def f2(x): return (abs(x-50))**0.5
def f3(x): return min(4*x,x+5)
def f4(x): return -x**3


#Q11
def nbrdor (f,a,b,epsilon):   #todo fix probleme de precision ?? (ça tombe souvent un tout petit peut à coté du vrai résultat, j'ai pas trouvé pourquoi)
    alpha=(1+5**0.5)/2
    xg = a+(b-a)*(1-1/alpha)
    xd = a+(b-a)/alpha

    fg=f(xg)
    fd=f(xd)

    while xd-xg > epsilon:
        if fg > fd :
            a=xg
            xg=xd
            xd=a+(b-a)/alpha
            fg=fd
            fd=f(xd)
        if fg < fd :
            b=xd
            xd=xg
            xg = a+(b-a)*(1-1/alpha)
            fd=fg
            fg=f(xg)
        else:
            #print (xg,xd)
            a=xg
            b=xd
            xg = a+(b-a)*(1-1/alpha)
            xd = a+(b-a)/alpha
            fg=f(xg)
            fd=f(xd)
        #print(xg,xd)
    return xg

#PARTIE 3

def conso(p,L,a,b): #Q12
    cout = 0
    for c in L:
        cout+= max(0,c-p)**2
    cout=b*(cout**0.5)+a*p
    return cout

def tracage(pts=100000,a=1,b=0.5,L=[2500, 3500, 4380, 4389, 4725, 4800, 3700, 3500, 7000, 7500, 2000, 1200]): #Q13
    def f(p): return (conso(p,L,a,b))
    X=np.linspace(0,max(L),pts)
    Y=np.array([f(p) for p in X])
    plt.plot(X,Y)
    plt.show()

def minconso(L,a,b,dico=nbrdor):
    def f(p): return (conso(p,L,a,b))
    C=max(L)
    return (dico(f,0,C,10**-7))


def testvitesse(n=400,tests=100):
    a=1
    b=0.5
    Ls = [[rd.randint(0,10000) for i in range (n)] for j in range (tests)]

    deb=time.time()
    for L in Ls:
        minconso(L,a,b,dichotomie)
    fin=time.time()
    durée=(fin-deb)/tests
    print("dichotomie : ",durée)

    deb=time.time()
    for L in Ls:
        minconso(L,a,b,nbrdor)
    fin=time.time()
    durée=(fin-deb)/tests
    print("nbrdor : ",durée)



testvitesse()










