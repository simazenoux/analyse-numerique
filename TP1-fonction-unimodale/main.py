import math
import numpy as np
import matplotlib.pyplot as plt
import time
import random as rd


def rechercheDichotomique(f, a, b, EPSILON):
    xc=a+(1/2)*(b-a)
    f_xc = f(xc)
    while (b-a > EPSILON):
        xg=a+(1/4)*(b-a)
        xd=a+(3/4)*(b-a)
        f_xg = f(xg)
        f_xd = f(xd)
        if f_xc > f_xd:
            a=xc
            xc=xd
            f_xc = f_xd
        elif f_xc > f_xg:
            b=xc
            xc=xg
            f_xc = f_xg
        else:
            a=xg
            b=xd

    return xc


def rechercheDichotomiqueNombreOr(f, a, b, EPSILON):
    alpha = (1+math.sqrt(5))/2
    xg = b - (b-a)/alpha
    xd = a + (b-a)/alpha
    f_xg = f(xg)
    f_xd = f(xd)
    while (b-a > EPSILON):
        if f_xg > f_xd:
            a=xg
            xg=xd
            f_xg=f_xd
            xd = a + (b-a)/alpha
            f_xd = f(xd)
        elif f_xg < f_xd:
            b=xd
            xd=xg
            f_xd=f_xg
            xg = b - (b-a)/alpha
            f_xg= f(xg)
        else:
            a=xg
            b=xd
            xg = b - (b-a)/alpha
            xd = a + (b-a)/alpha
            f_xg = f(xg)
            f_xd = f(xd)

    return (a+b)/2



def f1(x): return abs(x-100)
# Résultat attendu : f1(x)=0 <=> x=100
print(rechercheDichotomique(f1, -1000, 1000, pow(10, -7)))
print(rechercheDichotomiqueNombreOr(f1, -1000, 1000, pow(10, -7)))

def f2(x): return (abs(x-50))**0.5
# Résultat attendu : 50
print(rechercheDichotomique(f2, -1000, 1000, pow(10, -7)))
print(rechercheDichotomiqueNombreOr(f2, -1000, 1000, pow(10, -7)))

def f3(x): return min(4*x, x+5)
# Résultat attendu : -1000
print(rechercheDichotomique(f3, -1000, 1000, pow(10,-7)))
print(rechercheDichotomiqueNombreOr(f3, -1000, 1000, pow(10,-7)))

def f4(x): return pow(-x,3)
# Résultat attendu : -1000 (car -x^3 décroissante)
print(rechercheDichotomique(f4, -1000, 1000, pow(10,-7)))
print(rechercheDichotomiqueNombreOr(f4, -1000, 1000, pow(10,-7)))


# Exercice 3

def consommation(p,L,a,b):
    somme = 0
    for c in L:
        somme += max(0,c-p)**2
    return a*p + b*(math.sqrt(somme))


def affichage(a,b,L):
    def f(p): return (consommation(p,L,a,b))
    X=np.linspace(0,max(L))
    Y=np.array([f(p) for p in X])
    plt.plot(X,Y)
    plt.show()


def minRechercheDichotomique(L):
    def f(p): return (consommation(p,L,0,max(L)))
    return rechercheDichotomique(f, 0, max(L), pow(10, -7))

def minRechercheDichotomiqueNombreOr(L,a,b):
    def f(p): return (consommation(p,L,a,b))
    return rechercheDichotomiqueNombreOr(f, 0, max(L), pow(10, -7))



def testVitesse(n,tests):
    def f(p): return (consommation(p,L,a,b))
    a=1
    b=0.5
    EPSILON = pow(10, -7)
    
    Ls = [[rd.randint(0,10000) for i in range (n)] for j in range (tests)]

    deb=time.time()
    for L in Ls:
        rechercheDichotomique(f, a, b, EPSILON)
    fin=time.time()
    duree=(fin-deb)/tests
    print("Recherche dichotomique : ", duree)

    deb=time.time()
    for L in Ls:
        rechercheDichotomiqueNombreOr(f,a,b, EPSILON)
    fin=time.time()
    duree=(fin-deb)/tests
    print("Methode du nombre d'or : ", duree)

L=[2500, 3500, 4380, 4389, 4725, 4800, 3700, 3500, 7000, 7500, 2000, 1200]
a=1
b=0.5
affichage(a,b,L)
testVitesse(1000, 100)